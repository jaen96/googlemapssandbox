import {Component, OnInit, ViewChild} from '@angular/core'
import {GoogleMap, MapInfoWindow} from '@angular/google-maps'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  @ViewChild(GoogleMap, {static: false}) map: GoogleMap | undefined
  @ViewChild(MapInfoWindow, {static: false}) info: MapInfoWindow | undefined

  zoom = 12  // @ts-ignore
  center: google.maps.LatLngLiteral
  options: google.maps.MapOptions = {
    zoomControl: false,
    scrollwheel: false,
    disableDoubleClickZoom: true,
    mapTypeId: 'hybrid',
    maxZoom: 15,
    minZoom: 8,
  }
  markers: any[] = []
  address: string[] = ['Åsen 24', 'Gellerupvej 15']
  infoContent = ''

  ngOnInit() {
    navigator.geolocation.getCurrentPosition((position) => {
      this.center = {
        lat: position.coords.latitude,
        lng: position.coords.longitude,
      }
    })

    this.address.forEach(x => {
      this.getAddress(x);
    })
  }

  zoomIn() {
    if (this.options.maxZoom) {
      if (this.zoom < this.options.maxZoom) this.zoom++
    }
  }

  zoomOut() {
    if (this.options.minZoom)
      if (this.zoom > this.options.minZoom) this.zoom--
  }

  getAddress = (address: any) => {
    return new Promise((resolve, reject) => {
      const geocoder = new google.maps.Geocoder();
      geocoder.geocode({address: address}, (results, status) => {
        if (status === 'OK') {
          if (results) {
            resolve(results[0].geometry.location);
            console.log(results)
          }
        } else {
          reject(status);
        }
      }).then(location => {
        location.results.forEach(x => {
          this.markers.push({
            position: {
              lat: x.geometry.location.lat(),
              lng: x.geometry.location.lng()
            }
          })
        })
      });
    });
  };
}
